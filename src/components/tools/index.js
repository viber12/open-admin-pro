import Logo from './Logo.vue'
import Breadcrumb from './Breadcrumb.vue'
import DetailList from './DetailList.vue'
import HeadInfo from './HeadInfo.vue'
import UserMenu from './UserMenu.vue'

export {
  Logo,
  Breadcrumb,
  DetailList,
  HeadInfo,
  UserMenu
}
