const apis = {
  user: {
    page: '/admin/user',
    save: '/admin/user/save',
    updatePassword: '/admin/user/update/password',
    updateCurrent: '/admin/current/user/update',
    list: '/admin/user/list',
    saveUserRoles: '/admin/user/roles/save',
    getUserRoles: '/admin/user/roles'
  },
  role: {
    page: '/admin/role',
    save: '/admin/role/save',
    remove: '/admin/role/remove',
    list: '/admin/role/list',
    getRoleUsers: '/admin/role/users',
    saveRoleUsers: '/admin/role/users/save'
  },
  developer: {
    page: '/admin/developer',
    save: '/admin/developer/save',
    remove: '/admin/developer/remove',
    list: '/admin/developer/list',
    updatePassword: '/admin/developer/update/password'
  },
  app: {
    page: '/admin/app',
    save: '/admin/app/save',
    remove: '/admin/app/remove',
    saveClient: '/admin/app/client/save',
    resetClient: '/admin/app/reset',
    getApp: '/admin/app/info',
    getClient: '/admin/app/client/info'
  },
  api: {
    page: '/admin/api',
    add: '/admin/api/add',
    save: '/admin/api/save',
    list: '/admin/api/list',
    remove: '/admin/api/remove',
    batchRemove: '/admin/api/batch/remove',
    batchUpdate: '/admin/api/batch/update',
    batchUpdateServiceList: '/admin/api/batch/update/service/list',
    batchUpdateService: '/admin/api/batch/update/service',
    serviceList: '/admin/api/service/list'
  },
  menu: {
    save: '/admin/menu/save',
    remove: '/admin/menu/remove',
    list: '/admin/menu/list',
    getMenu: `/admin/menu/info`
  },
  action: {
    save: '/admin/action/save',
    remove: '/admin/action/remove',
    list: '/admin/action/list',
    getActionsByMenu: '/admin/menu/action'
  },
  authority: {
    user: '/admin/authority/user',
    role: '/admin/authority/role',
    app: '/admin/authority/app',
    menu: '/admin/authority/menu',
    api: '/admin/authority/api',
    action: '/admin/authority/action',
    userGrant: '/admin/authority/user/grant',
    roleGrant: '/admin/authority/role/grant',
    appGrant: '/admin/authority/app/grant',
    actionGrant: '/admin/authority/action/grant'
  },
  login: {
    login: '/admin/login/token',
    logout: '/admin/logout/token',
    getUserInfo: '/admin/current/user',
    getUserMenu: '/admin/current/user/menu',
    mobileLogin: '/admin/mobileLogin/token',
    getSmsCaptcha: '/sms/ captcha'
  },
  gateway: {
    refresh: '/actuator/open/refresh',
    getAccessLogs: '/admin/gateway/access/logs',
    getServiceList: '/admin/gateway/service/list'
  },
  routes: {
    page: '/admin/gateway/route',
    add: '/admin/gateway/route/add',
    save: '/admin/gateway/route/save'
  },
  rateLimit: {
    page: '/admin/gateway/limit/rate',
    save: '/admin/gateway/limit/rate/save',
    remove: '/admin/gateway/limit/rate/remove',
    getRateLimitApis: '/admin/gateway/limit/rate/api/list',
    saveRateLimitApis: '/admin/gateway/limit/rate/api/save'
  },
  ipLimit: {
    page: '/admin/gateway/limit/ip',
    save: '/admin/gateway/limit/ip/save',
    remove: '/admin/gateway/limit/ip/remove',
    getIpLimitApis: '/admin/gateway/limit/ip/api/list',
    saveIpLimitApis: '/admin/gateway/limit/ip/api/save'
  },
  jobs: {
    page: '/admin/job',
    save: '/admin/job/save',
    remove: '/admin/job/remove',
    pause: '/admin/job/pause',
    resume: '/admin/job/resume',
    getLogs: '/admin/job/logs'
  },
  msg: {
    webhook: {
      logs: '/admin/webhook/logs',
      resend: '/admin/webhook/resend'
    }
  },
  generate: {
    tables: '/admin/generate/tables',
    execute: '/admin/generate/execute',
    download: function () {
      return `${process.env.VUE_APP_API_BASE_URL}/code/generate/download`
    }
  }
}
export default apis
